const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
const router = require('./routes/index');
const bodyParser = require('body-parser')

app.use(cors());
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}));
app.use(bodyParser.json({limit: '100mb'}));
app.use('/', router);

app.listen(port, () => {
  console.log(`moules_ws listening on port ${port}`);
})
"use strict";

var util = require('util');

var needle = require('needle');

var authenticateUser = function authenticateUser(req, res, next) {
  var user = {
    id: '1',
    email: 'sinouhe@test.com',
    password: 'hehexd',
    firstName: 'sinouhe',
    lastName: 'soupart',
    token: 'token'
  };
  res.send(user);
};

var registerUser = function registerUser(req, res, next) {
  console.log('registerUser()');
  console.log('req.body:', util.inspect(req.body));
  var email = req.body.email;
  var password = req.body.password;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var user = {
    email: email,
    firstname: firstName,
    lastname: lastName
  };
  var body_data = {
    customer: user,
    password: password
  };
  var method = 'POST';
  var url = 'http://127.0.0.1/index.php/rest/all/V1/customers';
  var authorize_opt = {
    header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    json: true,
    multipart: true
  };
  return needle.request(method, url, body_data, authorize_opt, function (error, response, body) {
    console.log("Error:", error);
    console.log("response.body:", util.inspect(response.body));

    if (!error && response && ![400, 500].includes(response.statusCode)) {
      res.status(response.statusCode).json({
        success: true,
        message: 'user created successfully'
      });
    } else if (response) {
      res.status(response.statusCode).json({
        success: false,
        message: 'there was a problem creating the user',
        APIresponse: response.body
      });
    } else {
      res.status(500).json({
        success: false,
        message: 'there was a problem creating the user'
      });
    }
  });
};

var getUsers = function getUsers(req, res, next) {
  next();
};

var getUserById = function getUserById(req, res, next) {
  next();
};

var updateUserById = function updateUserById(req, res, next) {
  next();
};

var deleteUserById = function deleteUserById(req, res, next) {
  next();
};

var getListProducts = function getListProducts(req, res, next) {
  var method = 'GET';
  var url = 'http://127.0.0.1/index.php/rest/all/V1/products?searchCriteria=pageSize&pageSize=25';
  var body_data = {};
  var authorize_opt = {
    header: {
      'Accept': '*/*'
    },
    json: true
  };
  needle.request(method, url, body_data, authorize_opt, function (error, response, body) {
    console.log("Error:", error);
    console.log("response.body:", util.inspect(response.body));

    if (!error && response && ![400, 500].includes(response.statusCode)) {
      res.send(response.body);
    } else if (response) {
      res.status(response.statusCode).json({
        success: false,
        message: 'there was a problem retrieving the products',
        APIresponse: response.body
      });
    } else {
      res.status(500).json({
        success: false,
        message: 'there was a problem retrieving the products'
      });
    }
  });
};

var logger = function logger(req, res, next) {
  console.log("url:".concat(util.inspect(req.url)));
  next();
};

module.exports = {
  authenticateUser: authenticateUser,
  registerUser: registerUser,
  getUsers: getUsers,
  getUserById: getUserById,
  updateUserById: updateUserById,
  deleteUserById: deleteUserById,
  getListProducts: getListProducts,
  logger: logger
};
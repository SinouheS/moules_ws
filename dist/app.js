var express = require('express');
var app = express();
var port = 3000;
var cors = require('cors');
var router = require('./routes/index');
var bodyParser = require('body-parser');
app.use(cors());
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.json({ limit: '100mb' }));
app.use('/', router);
app.listen(port, function () {
    console.log("moules_ws listening on port " + port);
});

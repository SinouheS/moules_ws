const util = require('util');
const needle = require('needle');

const authenticateUser = (req, res, next) => {
    let user = {
        id: '1',
        email: 'sinouhe@test.com',
        password: 'hehexd',
        firstName: 'sinouhe',
        lastName: 'soupart',
        token: 'token'
    }
    res.send(user);
}

const registerUser = (req, res, next) => {
    console.log('registerUser()');
    console.log('req.body:', util.inspect(req.body));

    let email = req.body.email;
    let password = req.body.password;
    let firstName = req.body.firstName;
    let lastName = req.body.lastName;

    let user = {
        email: email,
        firstname: firstName,
        lastname: lastName
    }

    let body_data = { 
        customer: user, 
        password: password
    };
    let method = 'POST';
    let url = 'http://127.0.0.1/index.php/rest/all/V1/customers';
    let authorize_opt = {
        header: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        json: true,
        multipart: true
    }

    return needle.request(method, url, body_data, authorize_opt, (error, response, body) => {
        console.log("Error:", error);
        console.log("response.body:", util.inspect(response.body));

        if(!error && response && ![400, 500].includes(response.statusCode)) {
            res.status(response.statusCode).json({ 
                success: true,
                message: 'user created successfully'
            })
        } else if (response) {   
            res.status(response.statusCode).json({ 
                success: false,
                message: 'there was a problem creating the user',
                APIresponse: response.body
            })
        } else {
            res.status(500).json({ 
                success: false,
                message: 'there was a problem creating the user'
            })
        }
    });
}

const getUsers = (req, res, next) => {
    next();
}

const getUserById = (req, res, next) => {
    next();
}

const updateUserById = (req, res, next) => {
    next();
}

const deleteUserById = (req, res, next) => {
    next();
}

const getListProducts = (req, res, next) => {

    let method = 'GET';
    let url = 'http://127.0.0.1/index.php/rest/all/V1/products?searchCriteria=pageSize&pageSize=25';    
    let body_data = {};
    let authorize_opt = {
        header: {
            'Accept': '*/*'
        },
        json: true
    };

    needle.request(method, url, body_data, authorize_opt, (error, response, body) => {
        console.log("Error:", error);
        console.log("response.body:", util.inspect(response.body));

        if(!error && response && ![400, 500].includes(response.statusCode)) {
            res.send(response.body);
        } else if (response) {   
            res.status(response.statusCode).json({ 
                success: false,
                message: 'there was a problem retrieving the products',
                APIresponse: response.body
            });
        } else {
            res.status(500).json({ 
                success: false,
                message: 'there was a problem retrieving the products'
            });
        }
    });
}

const logger = (req, res, next) => {
    console.log(`url:${util.inspect(req.url)}`);
    next();
}

module.exports = {
    authenticateUser,
    registerUser,
    getUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    getListProducts,
    logger
}
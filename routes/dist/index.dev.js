"use strict";

var express = require('express');

var router = express.Router();

var controller = require('../controller'); // logger


router.use(controller.logger); // routes

router.post('/users/authenticate', controller.authenticateUser);
router.post('/users/register', controller.registerUser);
router.get('/users', controller.getUsers);
router.get('/users/:id', controller.getUserById);
router.put('/users/:id', controller.updateUserById);
router["delete"]('/users/:id', controller.deleteUserById);
router.get('/products', controller.getListProducts);
module.exports = router;